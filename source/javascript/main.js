let colors = ["red", "green", "blue", "yellow", "purple", "red", "green", "blue", "yellow", "purple", "grey", "grey", "orange", "orange", "cyan", "cyan", "violet", "violet", "pink", "pink"],
    loop = colors.length, 
    cardSet = [],
    controlArr = [],
    selectedCards = 0,
    tries = 0,
    score = 0,
    selectedOne,
    selectedTwo,
    ele1,
    ele2,
    hideMe = false,
    id;

function generate() {
  for (let i = 0; i < loop; i++) {
    let arraySelect = Math.floor(Math.random() * colors.length);
    let color = colors[arraySelect];
    cardSet[i] = color;
    colors.splice(arraySelect, 1);
  }
}

generate(); // run gen right away to generate game

function showCard(id, location) {
  document.getElementById(id).style.background = cardSet[location];
}

function hideCard(id) {
  document.getElementById(id).style.background = "white";
}

function printStats() {
  // Print score and tries
  document.getElementById("tries").innerHTML = tries;
  document.getElementById("score").innerHTML = score;

  // Check if win state is reached and print if true
  if (score === (cardSet.length / 2) && tries > 0) {
    document.getElementById("win").hidden = false;
  } else if (score === 0) {
    document.getElementById("win").hidden = true;
  }
}

function reset() {
  // Reset it all just in case
  colors = ["red", "green", "blue", "yellow", "purple", "red", "green", "blue", "yellow", "purple", "grey", "grey", "orange", "orange", "cyan", "cyan", "violet", "violet", "pink", "pink"];
  loop = colors.length; // for loop
  cardSet = [];
  controlArr = [];
  selectedCards = 0;
  tries = 0;
  score = 0;
  selectedOne;
  selectedTwo;
  ele1 = undefined;
  ele2 = undefined;
  hideMe = false;
  id = undefined;

  printStats(); //  Print score and tries of 0
  generate(); // generate a new game

  for (let i = 0; i < loop; i++) {
    document.getElementById("card_" + i).style.background = "white";
  }
}

function checkCard(ele, location) {
  // hide previously selected set of cards on next run but only if previous selection was non-matching
  if (hideMe === true) {
    hideCard(ele1);
    hideCard(ele2);
    hideMe = false;
  }

  // Check how many cards are selected and play apropriate
  if (selectedCards === 0) {
    selectedOne = location;
    id = ele.id;
    ele1 = id;
  } else if (selectedCards === 1) {
    selectedTwo = location;
    id = ele.id;
    ele2 = id;
    if (ele1 === ele2) {
      return;
    }
  }

  // Bug fix, prevents user from selecting already guessed card
  if (controlArr.includes(id)) {
    return;
  }

  // Show selected card and increment the card selected counter by one
  showCard(id, location);
  selectedCards++;

  // Once two cards are selected check if they match
  if (selectedCards === 2) {
    if (cardSet[selectedOne] === cardSet[selectedTwo]) {
      selectedCards = 0;
      tries++;
      score++;
      printStats();
      controlArr.push(ele1);
      controlArr.push(ele2);
    } else {
      selectedCards = 0;
      hideMe = true;
      tries++;
      printStats();
    }
  }
}
